<?php get_header();?>
    <div class="breadcrumb">
      <div class="container">
        <div class="breadcrumb-inner">
          <ul class="list-inline list-unstyled">
            <li><a href="#">Home</a></li>
            <li class='active'>Blog</li>
          </ul>
        </div>
      </div>
    </div>
    <div class="body-content">
      <div class="container">
        <div class="row">
          <div class="blog-page">
            <div class="col-md-9">
              <div class="blog-post  wow fadeInUp">
                <a href="blog-details.html"><img class="img-responsive" src="<?php echo get_template_directory_uri();?>/assets/images/blog-post/blog_big_01.jpg" alt=""></a>
                <h1><a href="blog-details.html">Nemo enim ipsam voluptatem quia voluptas sit aspernatur</a></h1>
                <span class="author">John Doe</span>
                <span class="review">6 Comments</span>
                <span class="date-time">14/06/2016 10.00AM</span>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum...</p>
                <a href="#" class="btn btn-upper btn-primary read-more">read more</a>
              </div>

            </div>
            <?php get_template_part('right-sidebar');?>
          </div>
        </div>
        <div id="brands-carousel" class="logo-slider wow fadeInUp">
          <div class="logo-slider-inner">
            <div id="brand-slider" class="owl-carousel brand-slider custom-carousel owl-theme">
              <div class="item m-t-15">
                <a href="#" class="image">
                <img data-echo="<?php echo get_template_directory_uri();?>/assets/images/brand4.png" src="<?php echo get_template_directory_uri();?>/assets/images/blank.gif" alt="">
                </a>	
              </div>
              <div class="item m-t-10">
                <a href="#" class="image">
                <img data-echo="<?php echo get_template_directory_uri();?>/assets/images/brand4.png" src="<?php echo get_template_directory_uri();?>/assets/images/blank.gif" alt="">
                </a>	
              </div>
              <div class="item">
                <a href="#" class="image">
                <img data-echo="<?php echo get_template_directory_uri();?>/assets/images/brand4.png" src="<?php echo get_template_directory_uri();?>/assets/images/blank.gif" alt="">
                </a>	
              </div>
              <div class="item">
                <a href="#" class="image">
                <img data-echo="<?php echo get_template_directory_uri();?>/assets/images/brand4.png" src="<?php echo get_template_directory_uri();?>/assets/images/blank.gif" alt="">
                </a>	
              </div>
              <div class="item">
                <a href="#" class="image">
                <img data-echo="<?php echo get_template_directory_uri();?>/assets/images/brand4.png" src="<?php echo get_template_directory_uri();?>/assets/images/blank.gif" alt="">
                </a>	
              </div>
              <div class="item">
                <a href="#" class="image">
                <img data-echo="<?php echo get_template_directory_uri();?>/assets/images/brand4.png" src="<?php echo get_template_directory_uri();?>/assets/images/blank.gif" alt="">
                </a>	
              </div>
              <div class="item">
                <a href="#" class="image">
                <img data-echo="<?php echo get_template_directory_uri();?>/assets/images/brand4.png" src="<?php echo get_template_directory_uri();?>/assets/images/blank.gif" alt="">
                </a>	
              </div>
              <div class="item">
                <a href="#" class="image">
                <img data-echo="<?php echo get_template_directory_uri();?>/assets/images/brand4.png" src="<?php echo get_template_directory_uri();?>/assets/images/blank.gif" alt="">
                </a>	
              </div>
              <div class="item">
                <a href="#" class="image">
                <img data-echo="<?php echo get_template_directory_uri();?>/assets/images/brand4.png" src="<?php echo get_template_directory_uri();?>/assets/images/blank.gif" alt="">
                </a>	
              </div>
              <div class="item">
                <a href="#" class="image">
                <img data-echo="<?php echo get_template_directory_uri();?>/assets/images/brand4.png" src="<?php echo get_template_directory_uri();?>/assets/images/blank.gif" alt="">
                </a>	
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php get_footer();?>
<?php
  function woo_commerce_theme_support(){
     add_theme_support('title-tag');
  }
add_action('after_setup_theme','woo_commerce_theme_support');
 function woo_commerce_css_js(){
    wp_enqueue_style('google-font-1','//fonts.googleapis.com/css?family=Roboto:300,400,500,700');
    wp_enqueue_style('google-font-2','//fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,600,600italic,700,700italic,800');
    wp_enqueue_style('google-font-3','//fonts.googleapis.com/css?family=Montserrat:400,700');
    wp_enqueue_style('bootstrap-css',get_template_directory_uri().'/assets/css/bootstrap.min.css');
    wp_enqueue_style('main-css',get_template_directory_uri().'/assets/css/main.css');
    wp_enqueue_style('blue-css',get_template_directory_uri().'/assets/css/blue.css');
    wp_enqueue_style('Owl-carosel-css',get_template_directory_uri().'/assets/css/owl.carousel.css');
    wp_enqueue_style('owl-transitions-css',get_template_directory_uri().'/assets/css/owl.transitions.css');
    wp_enqueue_style('animated-css',get_template_directory_uri().'/assets/css/animate.min.css');
    wp_enqueue_style('rateit-css',get_template_directory_uri().'/assets/css/rateit.css');
    wp_enqueue_style('bootstrap-select-min-css',get_template_directory_uri().'/assets/css/bootstrap-select.min.css');
    wp_enqueue_style('font-awesome',get_template_directory_uri().'/assets/css/font-awesome.css');
    wp_enqueue_style('woo-commerce-main-css',get_stylesheet_uri());


     wp_enqueue_script('jquery');
     wp_enqueue_script('bootstrap-min-js',get_template_directory_uri().'/assets/js/bootstrap.min.js',null,true);
     wp_enqueue_script('dropdown-min-js',get_template_directory_uri().'/assets/js/bootstrap-hover-dropdown.min.js','jquery',null,true);
     wp_enqueue_script('owl-carousel-min-js',get_template_directory_uri().'/assets/js/owl.carousel.min.js','jquery',null,true);
     wp_enqueue_script('echo-min-js',get_template_directory_uri().'/assets/js/echo.min.js','jquery',null,true);
     wp_enqueue_script('jquery-easing-13-min-js',get_template_directory_uri().'/assets/js/jquery.easing-1.3.min.js','jquery',null,false);
     wp_enqueue_script('bootstrap-slider-min',get_template_directory_uri().'/assets/js/bootstrap-slider.min.js','jquery',null,true);
     wp_enqueue_script('jquery-rateit-min',get_template_directory_uri().'/assets/js/jquery.rateit.min.js','jquery',null,true);
     wp_enqueue_script('lightbox-min-js',get_template_directory_uri().'/assets/js/lightbox.min.js','jquery',null,true);
     wp_enqueue_script('bootstrap-select-min',get_template_directory_uri().'/assets/js/bootstrap-select.min.js','jquery',null,true);
     wp_enqueue_script('wow-min-js',get_template_directory_uri().'/assets/js/wow.min.js','jquery',null,true);
     wp_enqueue_script('scripts-js',get_template_directory_uri().'/assets/js/scripts.js','jquery',null,true);


 }
add_action('wp_enqueue_scripts','woo_commerce_css_js');
?>